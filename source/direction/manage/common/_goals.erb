Through 2021, the Manage stage will provide such compelling, must-have value to large customers that we will be able to **attribute over $100M in ARR to Manage capabilities by end of year**. This means that Manage is a must-have part of the feature set that supports that customer, or Manage was a key part of their adoption journey.

### Goal Themes

#### Enterprise readiness
We're going to focus on increasing and retaining the number of customers with enterprise-scale needs. We're doing this by focusing on:

* Enterprise-grade authentication and authorization. We'll focus on SAML and build excellent compatibility and documentation with large identity providers. This should work on both GitLab.com and self-managed.
* Comprehensive audit events for everything that’s done within GitLab and allowing those events to be accessible via the API and UI.
* Isolation and control, especially for GitLab.com. For some organizations, there must be safeguards in place to prevent users from viewing or accessing other groups and projects. Providing isolation of [enterprise users](https://gitlab.com/groups/gitlab-org/-/epics/4786)] will help organizations better manage their GitLab usage by providing a more "instance-like" experience at the group level.

| Success factor | How we'll measure |
| ------ | ------ |
| Increased self-managed enterprise adoption | At least X customers over Y paid seats purchasing a self-managed license in 2021 |
| Increased GitLab.com enterprise adoption | At least 15 customers over 500 paid seats purchasing GitLab.com in 2021 |
| Increased engagement with enterprise must-have features | Increase in paid feature engagement for the enterprise: [Paid SAML](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#manageaccess---paid-gmau---mau-using-paid-saml) |
| Increased CSAT from large organizations | TBD |

##### GitLab.com
Of particular importance to our goal of enterprise readiness is ensuring that GitLab.com is ready to meet the needs of large organizations. By the end 

of 2021, we will close the following gaps between self-managed and GitLab.com:

| Problem | Approach |
| ------ | ------ |
| Some user events lack auditability and visibility. | Audit events are visible at the group level, including a [group-level API](https://docs.gitlab.com/ee/api/audit_events.html). We'll continue to add [additional audit events](https://gitlab.com/groups/gitlab-org/-/epics/736) and improve the [overall usability of these events](https://gitlab.com/groups/gitlab-org/-/epics/418) over time. |
| Individual members of an organization can fork enterprise projects into their personal namespace, introducing security concerns over IP controls. | Groups can already [prevent forks](https://gitlab.com/gitlab-org/gitlab/-/issues/216987) outside their namespace. In the future, [[enterprise users](https://gitlab.com/groups/gitlab-org/-/epics/4786) will allow organizations to manage project forks, even if they're in personal namespaces. |
| Organizations want to have traceability (and control) over the actions a user's account is taking. | While we don't have plans for an organization to have control over all actions a user's account is taking (a single user account can belong to multiple paid groups; defining account ownership in GitLab.com's current architecture is very challenging), we will allow an enterprise to have [visibility and manage user activity that is directly relevant to their organization's group](https://gitlab.com/groups/gitlab-org/-/epics/4345). |
| Since GitLab, Inc. employees are the only admins on GitLab.com, administrative controls for group owners are limited. | We're tentatively planning to pull instance-level admin activity into an [administrative area for group owners](https://gitlab.com/gitlab-org/gitlab/-/issues/209020). |
| Managing credentials (e.g. regular rotation) is not possible, since users belong to the instance and not a particular namespace. | We will allow users to create group specific credentials that can be managed by administrators in a group specific version of [Credentials Inventory](https://gitlab.com/groups/gitlab-org/-/epics/4123)|
| Since instance-level functionality is admin-only, group owners can't use any features built at this level. | [Workspaces and cascading settings](https://gitlab.com/groups/gitlab-org/-/epics/4419) will allow us to build features and settings at the group-level so they are accessible in both GitLab deployment models.|

If you'd like to comment on our GitLab.com approach, please consider opening an MR or leaving a comment in [this feedback issue](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17277).

#### Enhancing Ultimate
We're going to drive an Ultimate story that creates obvious, compelling value. The majority of Ultimate's value proposition lies in application security, but we'll strive to improve the breadth of this tier by driving more value in Ultimate, such as:

* Improving tools that help compliance-minded organizations thrive. GitLab makes it easy to contribute, but administrators should have comprehensive control to establish, enforce, and provide evidence of organizational policies that are part of a compliance program or framework. Our compliance vision will evolve to introduce features that enable organizations to rely on GitLab for the enforcement and documentation of policies they set.
* More customizable and fine-grained permissions. GitLab's RBAC permissions system works well for most, but we should offer more powerful customization for customers to leverage.
* Powerful analytical insights. Provide dashboarding and analytics for project and portfolio management, allowing business to track and communicate progress on work in flight, capacity of teams and projects, and overall efficiency across their full portfolio.

Success in this theme looks like:
* Increased share of IACV in Ultimate.
* Increased adoption and engagement with Ultimate-level features.

#### Easy adoption
Manage will create easy paths to support our land-and-expand strategy. There's a starting point for any organization with an expansive new tool, and Manage will make this transition easy by supporting natural starting points - ideally in Core, for all groups - that get our customers started and hooked on GitLab:
* Easier import at any scale. Large-scale moves to GitLab should be significantly easier. We'll particularly focus on the user experience migrating from 2-3 key competitors, including gracefully recovering from failures.
* Drive entry-level enterprise table stakes into Core. Each group will focus on a Core value proposition that allows every user to get value - and encourages enterprises testing the water to land (and later expand) in GitLab.

### Access
1. Currently, self-managed and GitLab.com have different authentication and authorization capabilities. Our goal for 2021 is to bring features to Gitlab.com to unblock adoption; resulting in at least 15 enterprise-scale customers over 500 paid seats to begin using GitLab.com in 2021.
2. Implement fine-grained user permissions for common enterprise use cases, particularly segregation of duties.
3. Build support for the top 5 SAML SSO and SCIM providers to reduce time and friction onboarding customers.

Access uses a [single epic](https://gitlab.com/groups/gitlab-org/-/epics/3134) to highlight issues we're prioritizing or refining. If you're not confident an important Access issue is on our roadmap, please feel free to highlight by commenting in the relevant issue and @ mentioning the relevant PM.

### Optimize
In order to reach our vision, we need to:
* Create at least 1 analytics feature that our customers consistently use. Our analytics features are still trying to find product-market fit; our goal is to find that fit by finding a feature that our customers find useful and return to.
* Solve 1 major problem for the large enterprise with an MVC and at least 1 iteration. The problem most organizations face is a lack of instance-level visibility and an unknown ROI from their GitLab deployment. We'd like to ship against this problem and validate that we're moving in the right direction with these target customers.

### Compliance
Towards this vision, the Compliance group at GitLab is focused on three key areas:
* Enabling organizations to enforce compliance controls inside of GitLab (e.g. Define MR approvals at a global level and only authorized users may change those settings), including separation of duties (described in more detail below)
* Aggregating evidence and other audit information in a way that's easy to obtain and read
* Ensuring a comprehensive level of traceability and auditability of GitLab using Audit Events

### Import
Following our vision, the Import group is focused on these goals for 2021:
* Achieving a Lovable GitLab-to-GitLab migration experience. Allow a self-managed instance to easily migrate to GitLab.com (and vice versa) and self-service a migration of projects, groups, users, and most other objects.
* Double our import activity from GitHub. We'll accomplish this by improving the import user experience, performance, and capabilities of our GitHub importer (such as extending our support for GitHub Enterprise and introducing the ability to import an entire [GitHub organization](https://gitlab.com/gitlab-org/gitlab/-/issues/16911)).
* Share of failed imports reduced by 50% (from X to Y)
